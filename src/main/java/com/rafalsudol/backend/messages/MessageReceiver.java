package com.rafalsudol.backend.messages;

import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class for receiving and evaluating JADE ACL messages
 *
 * @author Rafal Sudol
 */
public final class MessageReceiver {
    /**
     * agent to receive a message
     */
    private final Agent agent;

    /**
     * behaviour waiting for a message
     */
    private final Behaviour behaviour;

    /**
     * type of the performative
     */
    private final MessageBuilder.Performative performative;

    /**
     * type of the ontology
     */
    private final MessageBuilder.Ontology ontology;

    /**
     * template of a message
     */
    private MessageTemplate template;

    private final Logger logger = LoggerFactory.getLogger(getClass().getName());

    public MessageReceiver(Agent agent,
                           Behaviour behaviour,
                           MessageBuilder.Performative performative,
                           MessageBuilder.Ontology ontology) {
        this.agent = agent;
        this.behaviour = behaviour;
        this.performative = performative;
        this.ontology = ontology;
        this.template = createTemplate();
    }

    public MessageReceiver(Agent agent,
                           Behaviour behaviour,
                           MessageBuilder.Performative performative) {
        this.agent = agent;
        this.behaviour = behaviour;
        this.performative = performative;
        this.ontology = null;
        this.template = createTemplate();
    }

    /**
     * creates a message template
     */
    private MessageTemplate createTemplate() {
        logger.info("Creating a template...");
        MessageTemplate performativeTemplate = null;
        MessageTemplate ontologyTemplate = null;
        MessageTemplate result = null;

        if (this.performative != null) {
            performativeTemplate = MessageTemplate.MatchPerformative(this.performative.getPerformative());
        }
        if (this.ontology != null) {
            ontologyTemplate = MessageTemplate.MatchOntology(this.ontology.name());
        }

        if (performativeTemplate != null && ontologyTemplate != null) {
            result = MessageTemplate.and(performativeTemplate, ontologyTemplate);
            logger.info("Template with an ontology and performative created");
        } else {
            result = performativeTemplate;
            logger.info("Template with a performative created");
        }
        return result;
    }

    /**
     * processes a message with String content
     */
    public void executeWithString(MessageContentReceiver<String> task) {
        ACLMessage message = this.agent.receive(template);
        if (message != null) {
            logger.info("Message with a String content received, processing...");
            task.evaluate(message, message.getContent());
        } else {
            logger.info("Failed to receive a message with a String content");
            this.behaviour.block();
        }
    }

    /**
     * processes a message with Object content
     */
    public void executeWithObject(MessageContentReceiver<Object> task) throws UnreadableException {
        ACLMessage message = this.agent.receive(this.template);
        if (message != null) {
            logger.info("Message with an Obejct received, processing...");
            task.evaluate(message, message.getContentObject());
        } else {
            logger.info("Failed to receive a message with an Object content");
            this.behaviour.block();
        }
    }
}
