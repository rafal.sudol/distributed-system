package com.rafalsudol.backend.messages;

import jade.core.AID;
import jade.lang.acl.ACLMessage;
import jade.util.leap.Iterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

/**
 * Class for building JADE ACL messages
 *
 * @author Rafal Sudol
 */
public final class MessageBuilder {
    /**
     * an output message
     */
    private final ACLMessage message;

    /**
     * type of the performative
     */
    private final Performative performative;

    /**
     * type of the ontology
     */
    private final Ontology ontology;


    private final Logger logger = LoggerFactory.getLogger(getClass().getName());

    /**
     * enum representing a performative
     */
    public enum Performative {
        /**
         * a performative for informing purposes
         */
        INFORM(ACLMessage.INFORM),

        /**
         * a performative for requesting purposes
         */
        REQUEST(ACLMessage.REQUEST),

        /**
         * a performative for proposing purposes
         */
        PROPOSE(ACLMessage.PROPOSE),

        /**
         * a performative for accepting proposal purposes
         */
        ACCEPT_PROPOSAL(ACLMessage.ACCEPT_PROPOSAL);

        /**
         * a performative constant, coming from JADE framework
         */
        int performative;

        Performative(int performative) {
            this.performative = performative;
        }

        public int getPerformative() {
            return this.performative;
        }
    }

    /**
     * enum representing an ontology
     */
    public enum Ontology {
        /**
         * registration ontology, used by both an agent and a manager
         */
        REGISTRATION,

        /**
         * ontology used for agent's movements validation
         */
        MOVE,

        /**
         * ontology used for agent's movements validation requests
         */
        MOVE_REQUEST,

        /**
         * ontology used for agent's movements validation confirmations
         */
        MOVE_ANSWER,

        /**
         * ontology used for updating bombs' states and locations
         */
        BOMB_UPDATE,

        /**
         * ontology used by an agent to confirm an ability to help other agent with defusing a bomb
         */
        ANSWER_HELP,

        /**
         * ontology used by an agent to request other agents help with defusing a bomb
         */
        REQUEST_HELP
    }

    public MessageBuilder(Performative performative, Ontology ontology) {
        this.performative = performative;
        this.ontology = ontology;
        this.message = new ACLMessage(performative.getPerformative());
        this.message.setOntology(this.ontology.name());
    }

    public MessageBuilder(Performative performative, Ontology ontology, ACLMessage incomingMessage) {
        this.performative = performative;
        this.ontology = ontology;
        this.message = incomingMessage.createReply();
        this.message.setPerformative(performative.getPerformative());
        this.message.setOntology(this.ontology.name());
    }

    public ACLMessage getMessage() {
        return this.message;
    }

    /**
     * sets message's receiver(one receriver)
     *
     * @param receiver receiver's ID
     */
    public MessageBuilder setReceiver(AID receiver) {
        if (receiver != null) {
            boolean isNew = true;
            Iterator receiverIterator = this.message.getAllReceiver();
            AID nextReceiver = null;

            while (receiverIterator.hasNext()) {
                nextReceiver = (AID) receiverIterator.next();

                if (nextReceiver.equals(receiver)) {
                    isNew = false;
                }
            }

            if (isNew) {
                this.message.addReceiver(receiver);
                logger.info("dodano adresata: " + receiver.getName());
                return this;
            } else {
                logger.error("przekazany adresat jest juz na liscie adresatow");
                return null;
            }
        } else {
            logger.error("nie udalo sie dodac adresata, przekazany adresat to null");
            return null;
        }
    }

    /**
     * set message's receivers(many receivers)
     *
     * @param receiverList list of receivers' IDs
     */
    public MessageBuilder setReceiver(List<AID> receiverList) {
        for (AID receiver : receiverList) {
            setReceiver(receiver);
        }
        return this;
    }

    /**
     * sets String message content
     *
     * @param string content to be set
     */
    public MessageBuilder setMessageContent(String string) {
        this.message.setContent(string);
        return this;
    }

    /**
     * sets Object message content
     *
     * @param object content to be set
     */
    public MessageBuilder setMessageContent(Serializable object) throws IOException {
        this.message.setContentObject(object);
        return this;
    }
}
