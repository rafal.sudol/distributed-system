package com.rafalsudol.backend.messages;

import jade.lang.acl.ACLMessage;

/**
 * Interface for processing an ACL message
 *
 * @author Rafal Sudol
 */
public interface MessageContentReceiver<E> {
    void evaluate(ACLMessage message, E content);
}
