package com.rafalsudol.backend.behaviours.managerBehaviours;

import com.rafalsudol.backend.agents.Manager;
import com.rafalsudol.backend.agents.MyAgent;
import com.rafalsudol.backend.messages.MessageBuilder;
import com.rafalsudol.backend.messages.MessageReceiver;
import com.rafalsudol.backend.sendable.CoordinatesContainer;
import com.rafalsudol.backend.terrain.BuildingMap;
import com.rafalsudol.backend.terrain.Coordinates;
import com.rafalsudol.backend.terrain.Point;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.UnreadableException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class implementing supervising behaviour
 * A manager waits for a message with disposal unit's desired new position.
 * If the move is valid(does not collide with movement, that has been just performed
 * by some other disposal unit) confirms that movement can be performed.
 * Otherwise prevents the disposal unit from making the move
 * The behaviour is executed periodically.
 *
 * @author Rafal Sudol
 */
public final class Supervise extends TickerBehaviour {
    private final Logger logger = LoggerFactory.getLogger(getClass().getName());

    /**
     * message receiver for receiving messages regarding movement validation
     */
    private final MessageReceiver messageReceiver;

    /**
     * manager's building map
     */
    private final BuildingMap buildingMap;

    private CoordinatesContainer receivedCoordinatesContainer;


    public Supervise(BuildingMap buildingMap, Agent agent) {
        //behaviour is run every 1 second
        super(agent, 1000L);
        this.buildingMap = buildingMap;
        messageReceiver = new MessageReceiver(agent, this, MessageBuilder.Performative.REQUEST, MessageBuilder.Ontology.MOVE);
    }

    @Override
    protected void onTick() {
        logger.info("Waiting for a message with movement validation request...");
        try {
            messageReceiver.executeWithObject(((message, content) -> {
                receivedCoordinatesContainer = (CoordinatesContainer) content;
                //TODO add content validation
                myAgent.send(new MessageBuilder(MessageBuilder.Performative.INFORM, MessageBuilder.Ontology.MOVE)
                        .setReceiver(message.getSender())
                        .setMessageContent("ok")
                        .getMessage());
            }));
        } catch (UnreadableException e) {
            logger.error("Message sending/receiving failed");
            logger.error(e.toString());
        }
        if (receivedCoordinatesContainer != null) {
            updateMaps();
            ((MyAgent) myAgent).getBuildingMap().printMap();
            this.stop();
            myAgent.doDelete();
        } else {
            logger.info("For unknown reason message content refers to null");
        }

    }

    /**
     * updates manager's and GUI's map
     */
    public void updateMaps() {
        Coordinates oldPosition = receivedCoordinatesContainer.getOldCoordinates();
        Coordinates newPosition = receivedCoordinatesContainer.getNewCoordinates();

        buildingMap.getArea()[oldPosition.getYCor()][oldPosition.getXCor()].setState(Point.pointState.SAFE);
        ((Manager) myAgent).getMainController().getOriginalPoint(oldPosition.getXCor(), oldPosition.getYCor()).setState(Point.pointState.SAFE);
        ((Manager) myAgent).getMainController().getGuiBuildingMap().getBoard()[oldPosition.getYCor()][oldPosition.getXCor()].getPoint().setState(Point.pointState.SAFE);

        buildingMap.getArea()[newPosition.getYCor()][newPosition.getXCor()].setState(Point.pointState.AGENT);
        ((Manager) myAgent).getMainController().getOriginalPoint(newPosition.getXCor(), newPosition.getYCor()).setState(Point.pointState.AGENT);
        ((Manager) myAgent).getMainController().getGuiBuildingMap().getBoard()[newPosition.getYCor()][newPosition.getXCor()].getPoint().setState(Point.pointState.AGENT);

        ((Manager) myAgent).getMainController().getGuiBuildingMap().repaint();
    }
}
