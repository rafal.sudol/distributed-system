package com.rafalsudol.backend.behaviours.managerBehaviours;

import com.rafalsudol.backend.agents.MyAgent;
import com.rafalsudol.backend.behaviours.disposalUnitBehaviours.Move;
import com.rafalsudol.backend.terrain.BuildingMap;
import com.rafalsudol.backend.messages.MessageBuilder;
import com.rafalsudol.backend.messages.MessageReceiver;
import com.rafalsudol.backend.sendable.DataContainer;
import com.rafalsudol.backend.terrain.Coordinates;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

/**
 * Class implementing behaviour for registering disposal units in the system
 * A manager waits for a message with disposal unit's registration request.
 * If one is received, managers sends back disposal unit's initial position coordinates
 * and default direction in which the unit should explore an area to be examined.
 * Only limited number of disposal units can be registered, the limit is defined by the number
 * of units, specified in the GUI.
 * The behaviour is executed periodically.
 *
 * @author Rafal Sudol
 */
public final class ListenForUnits extends TickerBehaviour {
    private final Logger logger = LoggerFactory.getLogger(getClass().getName());
    /**
     * a MessageReceiver for receiving messages regarding disposal units' registration requests
     */
    private final MessageReceiver messageReceiver;

    /**
     * reference to Manager's BuildingMap
     */
    private final BuildingMap buildingMap;

    /**
     * default direction of movement used by disposal unit during exploring the area
     */
    private Move.Direction direction;

    /**
     * list of all initial positions of disposal units
     */
    private final List<Coordinates> dUInitialPositions;

    /**
     * constructor
     *
     * @param a           agent using this behaviour
     * @param period      period to execute this behaviour
     * @param buildingMap area to be examined
     */
    public ListenForUnits(Agent a, long period, BuildingMap buildingMap) {
        super(a, period);
        this.messageReceiver = new MessageReceiver(myAgent, this, MessageBuilder.Performative.REQUEST, MessageBuilder.Ontology.REGISTRATION);
        this.buildingMap = buildingMap;
        dUInitialPositions = buildingMap.checkForAgents();
        logger.info("Listing dispoal units' initial positions: ");
        for (Coordinates coordinates : dUInitialPositions) {
            logger.info(coordinates.toString());
        }
    }

    @Override
    protected void onTick() {
        logger.info("Waiting for a new disposal unit's registration request...");
        this.messageReceiver.executeWithString(((message, content) -> {
            if (content.equalsIgnoreCase("register please")) {
                logger.info("Request received, registering disposal unit in the system and sending back the data");
                try {
                    AID disposalUnit = message.getSender();
                    if (direction == null) {
                        direction = Move.Direction.N;
                    } else {
                        direction = direction.next();
                    }
                    BuildingMap mapToBeSent = ((MyAgent) myAgent).getBuildingMap();
                    Coordinates coordinatesToBeSent = dUInitialPositions.get(0);
                    DataContainer briefing = new DataContainer(mapToBeSent, direction, coordinatesToBeSent);
                    dUInitialPositions.remove(coordinatesToBeSent);
                    ACLMessage briefingMessage = new MessageBuilder(MessageBuilder.Performative.INFORM, MessageBuilder.Ontology.REGISTRATION)
                            .setReceiver(disposalUnit)
                            .setMessageContent(briefing)
                            .getMessage();
                    myAgent.send(briefingMessage);
                } catch (IOException e) {
                    logger.error("Sending data failed");
                    logger.error(e.toString());
                }
            }
        }));
        if (dUInitialPositions.size() == 0) {
            logger.info("Maximum number of disposal units already reached, cannot register next one.");
            stop();
        }
    }
}
