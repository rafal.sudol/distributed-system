package com.rafalsudol.backend.behaviours.common;

/**
 * class implements manager's initial(startup) behaviour
 *
 * @author Rafal Sudol
 */
public final class ManagerInitialReport extends InitialReport {

    @Override
    public void action() {
        //TODO develop if necessary
    }
}
