package com.rafalsudol.backend.behaviours.common;

import com.rafalsudol.backend.agents.DisposalUnit;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * class implements common initial behaviour
 *
 * @author Rafal Sudol
 */
public class InitialReport extends OneShotBehaviour {

    //TODO try to remove hardcoding
    private final static Logger LOGGER = LoggerFactory.getLogger(InitialReport.class);

    @Override
    public void action() {
        LOGGER.info("action method from InitialReport");
        List<AID> managers = ((DisposalUnit) myAgent).getManagers();
        LOGGER.info("Listing all managers...");
        for (AID manager : managers) {
            LOGGER.info(manager.getLocalName());
        }
    }
}
