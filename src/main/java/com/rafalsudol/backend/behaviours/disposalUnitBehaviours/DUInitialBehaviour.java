package com.rafalsudol.backend.behaviours.disposalUnitBehaviours;

import com.rafalsudol.backend.agents.DisposalUnit;
import com.rafalsudol.backend.messages.MessageBuilder;
import com.rafalsudol.backend.messages.MessageReceiver;
import com.rafalsudol.backend.sendable.DataContainer;
import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.UnreadableException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Class implementing initial behaviour of a disposal unit.
 * The unit registers to manager and gets a briefing from him.
 * The behaviour is executed only once at the startup.
 *
 * @author Rafal Sudol
 */
public final class DUInitialBehaviour extends Behaviour {
    private final Logger logger = LoggerFactory.getLogger(getClass().getName());

    /**
     * list of all managers
     */
    private final List<AID> managers;

    /**
     * state of the disposal unit
     */
    private State state;

    /**
     * enum representing disposal unit's state during this behaviour
     */
    private enum State {
        /**
         * sending registration request
         */
        REGISTER,

        /**
         * waiting for a briefing
         */
        RECEIVE_BRIEFING,

        /**
         * adding other behaviours
         */
        ADD_BEHAVIOURS,

        /**
         * the end of this behaviour
         */
        DONE
    }

    /**
     * constructor
     *
     * @param managers list of all managers
     */
    public DUInitialBehaviour(List<AID> managers) {
        this.managers = managers;
        this.state = State.REGISTER;
    }

    @Override
    public void action() {
        switch (this.state) {
            case REGISTER:
                logger.info(myAgent.getName() + "Trying to register...");
                myAgent.send(new MessageBuilder(MessageBuilder.Performative.REQUEST, MessageBuilder.Ontology.REGISTRATION).
                        setReceiver(this.managers).
                        setMessageContent("register please").
                        getMessage());
                logger.info("Message sent.");
                this.state = State.RECEIVE_BRIEFING;
                break;

            case RECEIVE_BRIEFING:
                logger.info(myAgent.getName() + ": waiting for a briefing message.");
                try {
                    MessageReceiver brefingMessageReceiver = new MessageReceiver(myAgent, this, MessageBuilder.Performative.INFORM, MessageBuilder.Ontology.REGISTRATION);
                    brefingMessageReceiver.executeWithObject((message, briefing) -> {
                        ((DisposalUnit) myAgent).setBuildingMap(((DataContainer) briefing).getBuildingMap());
                        ((DisposalUnit) myAgent).setPreferredDirection(((DataContainer) briefing).getDirection());
                        ((DisposalUnit) myAgent).setPosition(((DataContainer) briefing).getDuCoordinates());
                    });
                } catch (UnreadableException e) {
                    logger.error("Receiving a briefing failed.");
                }
                if (((DisposalUnit) myAgent).getBuildingMap() != null && ((DisposalUnit) myAgent).getPreferredDirection() != null && ((DisposalUnit) myAgent).getPosition() != null) {
                    logger.info("Received preferred direction: " + ((DisposalUnit) myAgent).getPreferredDirection());
                    logger.info("Received position: " + ((DisposalUnit) myAgent).getPosition());
                    this.state = State.ADD_BEHAVIOURS;
                }
                break;

            case ADD_BEHAVIOURS:
                logger.info(myAgent.getName() + " adding other behaviours.");
                myAgent.addBehaviour(new Move(myAgent, ((DisposalUnit) myAgent).getBuildingMap(), ((DisposalUnit) myAgent).getManagers()));
                this.state = State.DONE;
                break;
        }
    }

    @Override
    public boolean done() {
        return this.state == State.DONE;
    }
}
