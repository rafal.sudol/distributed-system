package com.rafalsudol.backend.behaviours.disposalUnitBehaviours;

import com.rafalsudol.backend.agents.DisposalUnit;
import com.rafalsudol.backend.messages.MessageBuilder;
import com.rafalsudol.backend.messages.MessageReceiver;
import com.rafalsudol.backend.sendable.CoordinatesContainer;
import com.rafalsudol.backend.terrain.Bomb;
import com.rafalsudol.backend.terrain.BuildingMap;
import com.rafalsudol.backend.terrain.Coordinates;
import com.rafalsudol.backend.terrain.Point;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;

/**
 * Class implementing moving behaviour of a disposal unit.
 * Includes methods describing way of moving and finding next positions to go to
 * The behaviour is executed periodically.
 *
 * @author Rafal Sudol
 */
public final class Move extends TickerBehaviour {
    private final Logger logger = LoggerFactory.getLogger(getClass().getName());

    /**
     * reference to disposal unit's map of area to be examined
     */
    private final BuildingMap buildingMap;

    /**
     * list containing all managers ID's
     */
    private final List<AID> managers;

    /**
     * coordinates of a new position to go to
     */
    private Coordinates nextMove;

    /**
     * enum representing directions in 2D space
     */
    public enum Direction {
        /**
         * north
         */
        N,

        /**
         * south
         */
        S,

        /**
         * west
         */
        W,

        /**
         * east
         */
        E;

        /**
         * returns direction following this direction, accordingly to the order in which directions are defined
         *
         * @return following direction
         */
        public Direction next() {
            Direction[] values = values();
            return values[(this.ordinal() + 1) % values.length];
        }
    }

    /**
     * constructor
     * period for executing this behaviour is 1 second
     *
     * @param agent       agent executing this behaviour
     * @param buildingMap map of area to be examined
     * @param managers    list of all managers
     */
    public Move(Agent agent, BuildingMap buildingMap, List<AID> managers) {
        super(agent, 1000);
        this.buildingMap = buildingMap;
        this.managers = managers;
    }

    /**
     * whole behaviour is quite complex one, it is split into three parts to improve the whole application performance.
     * These parts are:
     * - TRYING_TO_MOVE- disposal units defines its next position to go to
     * - WAITING_FOR_AUTHORIZATION- disposal units waits for manager's authorization of the newly defined position to go to
     * - MOVING- disposal unit performs all actions connected with moving to the new position
     */
    @Override
    protected void onTick() {
        switch (((DisposalUnit) myAgent).getDuState()) {
            case TRYING_TO_MOVE:
                nextMove = getNextMove();
                if (nextMove != null) {
                    logger.info("Next position to go to: " + nextMove);
                    requestAuthorization(nextMove);
                    logger.info("Authorization request sent.");
                    ((DisposalUnit) myAgent).setDuState(DisposalUnit.duState.WAITING_FOR_AUTHORIZATION);
                } else {
                    logger.error("Failed in finding new position to move to.");
                    //TODO add handling nextMove = null scenario
                }
                break;
            case WAITING_FOR_AUTHORIZATION:
                waitForAuthorization();
                break;
            case MOVING:
                logger.info("Moving to the new position...");
                move(nextMove);
                ((DisposalUnit) myAgent).setDuState(DisposalUnit.duState.TRYING_TO_MOVE);
                myAgent.doDelete();
                break;
        }
    }

    /**
     * finds possible next positions to move,
     * assumptions are based only on coordinates validity and points' states
     *
     * @param area           area to be examined, represented by array of Points
     * @param dUCoordinates  current disposal unit's position
     * @param requiredStates point's state allowing making move to that point
     * @return map of possible moves, the key is direction of the next possible move
     * and the value are coordinates of that move. Null is returned when current disposal units are invalid
     * or method parameters are null
     */
    private Map<Direction, Coordinates> possibleMoves(Point[][] area, Coordinates dUCoordinates, List<Point.pointState> requiredStates) {
        if (area != null && dUCoordinates != null) {
            Map<Direction, Coordinates> moves = new HashMap<>();
            int dUXCor = dUCoordinates.getXCor();
            int dUYCor = dUCoordinates.getYCor();

            if (dUXCor >= 0 && dUXCor < area[0].length && dUYCor >= 0 && dUYCor < area.length) {
                if (dUXCor > 0 && requiredStates.contains(area[dUYCor][dUXCor - 1].getState())) {
                    moves.put(Direction.W, new Coordinates(dUXCor - 1, dUYCor));
                }
                if (dUXCor < (area[0].length - 1) && requiredStates.contains(area[dUYCor][dUXCor + 1].getState())) {
                    moves.put(Direction.E, new Coordinates(dUXCor + 1, dUYCor));
                }
                if (dUYCor > 0 && requiredStates.contains(area[dUYCor - 1][dUXCor].getState())) {
                    moves.put(Direction.N, new Coordinates(dUXCor, dUYCor - 1));
                }
                if (dUYCor < (area.length - 1) && requiredStates.contains(area[dUYCor + 1][dUXCor].getState())) {
                    moves.put(Direction.S, new Coordinates(dUXCor, dUYCor + 1));
                }
                return moves;
            } else {
                logger.error("Coordinates out of the range.");
                return null;
            }
        } else {
            logger.error("Area or dUCoordinates references to null.");
            return null;
        }
    }

    /**
     * Finds coordinates of the next point to move to in order to get to a specified distant position.
     * If straight-line movement is not possible, method picks coordinates from possible options at random.
     *
     * @param from          starting point
     * @param to            destination point
     * @param optionsToMove possible moves
     * @return coordinates of the next move on the path to the destination point,
     * null when no movement is possible or when some of the method parameter refers to null
     */
    private Coordinates goTowards(Coordinates from, Coordinates to, Map<Direction, Coordinates> optionsToMove) {
        if (from != null && to != null && optionsToMove != null) {
            if (!to.sameCoordinates(from)) {
                Direction direction = defineDirection(from, to);
                if (optionsToMove.size() > 0) {
                    logger.info("Possible movements found, returning coordinates...");
                    return optionsToMove.keySet().contains(direction) ? optionsToMove.get(direction) : optionsToMove.get(optionsToMove.keySet().toArray()[new Random().nextInt(optionsToMove.size())]);
                } else {
                    logger.info("No movement is possible, returning null.");
                    return null;
                }
            } else {
                logger.error("Starting point and destination point have to be different, returning null.");
                return null;
            }
        } else {
            logger.error("Some parameter(s) refers to null, returning null.");
            return null;
        }
    }

    /**
     * Finds coordinates of the next point to move to in order to get to a specified distant position at the default map.
     * If straight-line movement is not possible, method picks coordinates from possible options at random.
     *
     * @param from starting point
     * @param to   destination point
     * @return coordinates of the next move on the path to the destination point,
     * null when no movement is possible or when some of the method parameter refers to null
     */
    private Coordinates goTowards(Coordinates from, Coordinates to) {
        return goTowards(from, to, possibleMoves(this.buildingMap.getArea(), from, Point.accessibleStates()));
    }

    /**
     * checks for an active bomb at the specified position
     *
     * @param coordinates position to look for a bomb at
     * @return true, when an active bomb is found, false when no bomb is found or when the bomb has already been defused
     */
    private boolean isActBombFound(Coordinates coordinates) {
        Bomb bomb = this.buildingMap.getArea()[coordinates.getYCor()][coordinates.getXCor()].getBomb();
        if (bomb != null && !bomb.isDefused()) {
            return true;
        }
        return false;
    }

    /**
     * finds coordinates of the next move, takes into account many different factors.
     * If only one possible movement is available, the method picks it.
     * When more than one possible movement is available:
     * - when the disposal unit is on its way to defuse a bomb it picks a point in any accessible state
     * on the path to the destination point
     * - when the disposal unit's destination point is not specified, the unit explores the area.
     * Points laying in the disposal unit's default direction are picked in the first place
     *
     * @return coordinates of the next point to move to, null when movement is not possible or when some problem has occurred.
     */
    private Coordinates getNextMove() {
        //finds all posiible movements(only points with accessible states)

        //disposal unit's current position
        Coordinates dUPosition = ((DisposalUnit) myAgent).getPosition();

        //disposal unit's destination
        Coordinates dUDestination = ((DisposalUnit) myAgent).getDestination();

        //disposal unit's preferred direction of movement
        Direction dUPrefDir = ((DisposalUnit) myAgent).getPreferredDirection();

        //map of the possible moves
        Map<Direction, Coordinates> possibleMoves = possibleMoves(this.buildingMap.getArea(), /*((DisposalUnit) myAgent).getPosition()*/dUPosition, Point.accessibleStates());

        //coordinates to be returned
        Coordinates newCoordinates;

        //if possibleMoves == null, null is returned
        if (possibleMoves == null) {
            logger.info("Something has gone wrong, possibleMoves = null, returning null.");
            return null;

            //when there is no possible move, null is returned
        } else if (possibleMoves.size() == 0) {
            logger.info("No possible move found, returning null.");
            return null;

            //when there is only one possible move, the move is chosen
        } else if (possibleMoves.size() == 1) {
            logger.info("Only one possible move found, returning coordinates...");
            return possibleMoves.values().iterator().next();

            /////////////////////////////////////////////////////////////////////////////////

            //many options to move

        } else {
            //when disposal unit is on its way to defuse a bomb
            //any point in an accessible state, that lays on the path to the destination point
            if (dUDestination != null) {
                newCoordinates = goTowards(dUPosition, dUDestination, possibleMoves);
                if (newCoordinates != null) {
                    logger.info("New point in the path to the destination point has been obtained");
                } else {
                    logger.info("Problem with finding point in the path");
                }
                return newCoordinates;
            }
            //TODO case when another bomb is found when the disposal units is on its way to defuse a bomb

            //if no destination is specified, the disposal unit explores the area
            //heads toward the closest undiscovered point
            Map<Direction, Coordinates> undiscoveredMoves = possibleMoves(buildingMap.getArea(), dUPosition, Point.discoverableStates());
            if (undiscoveredMoves == null) {
                logger.error("Something has gone wrong, undiscoveredMoves = null, returning null.");
                return null;
            }

            //when no undiscovered point lays in the direct neighbourhood, the closest one is found and picked
            if (undiscoveredMoves.size() == 0) {
                logger.info("No undiscovered point in the direct neighbourhood.");
                logger.info("Searching for the closest undiscovered...");
                Map<Direction, List<Coordinates>> closestUndiscovered = findClosestUndiscovered(dUPosition);

                //closestUndiscovered == null means the whole area has been already discovered
                if (closestUndiscovered == null) {
                    logger.info("Map exploration has been completed. Killing the disposal unit.");
                    myAgent.doDelete();
                    return null;
                }

                //undiscovered points laying in disposal unit's preferred directions are picked in the first place
                //if there is more than 1 such a point, coordinates to be returned are picked at random
                //when there is no possible move in that direction, the direction is picked a random
                // if there is more than 1 point in randomly picked direction, the coordinates are chosen at random
                if (closestUndiscovered.keySet().contains(dUPrefDir)) {
                    List<Coordinates> closestsList = closestUndiscovered.get(dUPrefDir);
                    if (closestsList.size() > 1) {
                        return closestsList.get(new Random().nextInt(closestsList.size()));
                    } else {
                        return closestsList.get(0);
                    }
                } else {
                    Direction randDir = (Direction) closestUndiscovered.keySet().toArray()[new Random().nextInt(closestUndiscovered.size())];
                    List<Coordinates> closestsList = closestUndiscovered.get(randDir);
                    if (closestsList.size() > 1) {
                        return closestsList.get(new Random().nextInt(closestsList.size()));
                    } else {
                        return closestsList.get(0);
                    }
                }
            }

            //when there is only one possible move to the undiscovered position, the move is picked
            if (undiscoveredMoves.size() == 1) {
                Direction newDir = undiscoveredMoves.keySet().iterator().next();
                newCoordinates = undiscoveredMoves.get(newDir);
                logger.info("Only one possible move to the undiscovered position: " + newDir + ", " + newCoordinates.toString());
                return newCoordinates;
            }

            //when there is more than 1 possible move to the undiscovered position
            //and there is a possible move in the disposal unit's preferred direction, the move is picked
            //otherwise coordinates are picked at random
            if (undiscoveredMoves.size() > 1) {
                if (undiscoveredMoves.keySet().contains(dUPrefDir)) {
                    logger.info("Many possible moves, move in the disposal unit's preferred direction picked.");
                    return undiscoveredMoves.get(dUPrefDir);
                } else {
                    logger.info("Many possible moves, move in the disposal unit's preferred direction not possible.");
                    Direction newDir = (Direction) undiscoveredMoves.keySet().toArray()[new Random().nextInt(undiscoveredMoves.keySet().size())];
                    newCoordinates = undiscoveredMoves.get(newDir);
                    return newCoordinates;
                }
            }
        }
        return null;
    }

    /**
     * finds closest undiscovered points
     *
     * @param dUPosition disposal unit's current position
     * @return map of undiscovered points, the key is the direction in which the point lays, the value are coordinates of that point,
     * null when method argument is invalid or when the whole are has already been discovered
     */
    private Map<Direction, List<Coordinates>> findClosestUndiscovered(Coordinates dUPosition) {
        if (dUPosition != null) {
            Map<Direction, Coordinates> initPossibleMoves = possibleMoves(this.buildingMap.getArea(), dUPosition, Point.accessibleStates());

            //temp map used only in this method
            BuildingMap tmpBuildingMap = this.buildingMap.getCopy();
            tmpBuildingMap.convertToChecked();
            Point[][] tmpArea = tmpBuildingMap.getArea();

            //when there are no undiscovered points in the direct neighbourhood of the unit
            //it means that there are only points with SAFE, AGENT, OBSTACLE, DEFUSED_BOMB states

            boolean found = false;

            //map of points to be returned
            Map<Direction, List<Coordinates>> finalClosestsMap = new HashMap<>();
            List<Coordinates> finalClosests = new ArrayList<>();
            List<Coordinates> nIterPossMoves = new ArrayList<>();
            List<Coordinates> np1IterPossMoves = new ArrayList<>();

            //initialization needed only for the first iteration of the loop
            for (Direction direction : initPossibleMoves.keySet()) {
                nIterPossMoves.add(initPossibleMoves.get(direction));
            }

            //check for undiscovered points in the direct neighbourhood of the unit
            for (Coordinates pos : nIterPossMoves) {
                if (tmpArea[pos.getYCor()][pos.getXCor()].getState() == Point.pointState.UNDISCOVERED) {
                    finalClosests.add(pos);
                    found = true;
                }
            }

            //there is no undiscovered point in the direct neighbourhood of the unit
            while (!found) {
                for (Coordinates coor : nIterPossMoves) {

                    //setting information that a point has already been checked
                    tmpArea[coor.getYCor()][coor.getXCor()].setState(Point.pointState.CHECKED);
                    Map<Direction, Coordinates> newPossMoves = possibleMoves(tmpArea, coor, Point.discoverableStates());
                    if (newPossMoves.size() > 0) {

                        //undiscovered point(s) found
                        found = true;
                        for (Direction dir : newPossMoves.keySet()) {
                            Coordinates coorToAdd = newPossMoves.get(dir);
                            Direction dirToCoorToAdd = defineDirection(dUPosition, coorToAdd);
                            if (finalClosestsMap.keySet().contains(dirToCoorToAdd)) {
                                List<Coordinates> existingList = finalClosestsMap.get(dirToCoorToAdd);
                                if (!existingList.contains(coorToAdd)) {
                                    existingList.add(coorToAdd);
                                }
                            } else {
                                List<Coordinates> newList = new ArrayList<>();
                                newList.add(coorToAdd);
                                finalClosestsMap.put(dirToCoorToAdd, newList);
                            }
                        }
                    } else {

                        //undiscovered point not found in the direct neighbourhood of the point, from which it was looked for
                        newPossMoves = possibleMoves(tmpArea, coor, Point.accessibleStates());
                        if (newPossMoves.size() == 0) {
                            logger.info("No possible move from this point.");
                        } else {
                            for (Direction dir : newPossMoves.keySet()) {
                                if (!np1IterPossMoves.contains(newPossMoves.get(dir))) {
                                    np1IterPossMoves.add(newPossMoves.get(dir));
                                }
                            }
                        }
                    }
                }
                nIterPossMoves = np1IterPossMoves;
                np1IterPossMoves = new ArrayList<>();

                if (nIterPossMoves.size() == 0) {
                    logger.info("No undiscovered point found, the area completely explored. Returning null.");
                    return null;
                }

            }
            return finalClosestsMap;
        } else {
            logger.error("possibleMoves == null, returning null.");
            return null;
        }
    }

    /**
     * finds a direction to go to in order to get to the specified destination
     *
     * @param from starting point
     * @param to   destination point
     * @return desired direction
     */
    private Direction defineDirection(Coordinates from, Coordinates to) {
        Direction direction;
        if (Math.abs(to.getXCor() - from.getXCor()) < Math.abs(to.getYCor() - from.getYCor())) {
            direction = to.getYCor() > from.getYCor() ? Direction.S : Direction.N;
        } else {
            direction = to.getXCor() > from.getXCor() ? Direction.E : Direction.W;
        }
        return direction;
    }

    /**
     * sends a move authorization request to the manager
     *
     * @param newPosition new disposal unit's position to be authorized
     */
    private void requestAuthorization(Coordinates newPosition) {
        CoordinatesContainer coordinatesContainer = new CoordinatesContainer(((DisposalUnit) myAgent).getPosition(), newPosition);
        logger.info("Sending move authorization request...");
        try {
            myAgent.send(new MessageBuilder(MessageBuilder.Performative.REQUEST, MessageBuilder.Ontology.MOVE)
                    .setReceiver(managers)
                    .setMessageContent(/*newPosition*/coordinatesContainer)
                    .getMessage());
        } catch (IOException e) {
            logger.error("Sending message failed.");
            logger.error(e.toString());
        }

    }

    /**
     * waits for and receives authorization message from the manager
     */
    private void waitForAuthorization() {
        logger.info("Waiting for the move authorization...");
        MessageReceiver authorizationMessageReceiver = new MessageReceiver(myAgent, this, MessageBuilder.Performative.INFORM, MessageBuilder.Ontology.MOVE);
        authorizationMessageReceiver.executeWithString((message, content) -> {
            if (content.equalsIgnoreCase("ok")) {
                logger.info("Move authorized, making the move...");
                ((DisposalUnit) myAgent).setDuState(DisposalUnit.duState.MOVING);
            } else {
                logger.info("The move has not been authorized, trying to find a new one...");
                ((DisposalUnit) myAgent).setDuState(DisposalUnit.duState.TRYING_TO_MOVE);
                //TODO prevent authorization of the same position attempts
            }
        });
    }

    /**
     * makes a move that has been authorized
     */
    private void move(Coordinates nextMove) {
        DisposalUnit du = (DisposalUnit) myAgent;
        du.getDUBuildingMap().getArea()[du.getPosition().getYCor()][du.getPosition().getXCor()].setState(Point.pointState.SAFE);
        du.setPosition(new Coordinates(nextMove.getXCor(), nextMove.getYCor()));
        du.getDUBuildingMap().getArea()[du.getPosition().getYCor()][du.getPosition().getXCor()].setState(Point.pointState.AGENT);
        du.getDUBuildingMap().printMap();
    }
}

