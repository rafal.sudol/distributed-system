package com.rafalsudol.backend.sendable;

import com.rafalsudol.backend.behaviours.disposalUnitBehaviours.Move;
import com.rafalsudol.backend.terrain.BuildingMap;
import com.rafalsudol.backend.terrain.Coordinates;

import java.io.Serializable;

/**
 * Class provides container for exchanging building map, direction and coordinates
 *
 * @author Rafal Sudol
 */
public final class DataContainer implements Serializable {
    /**
     * building map to be sent
     */
    private final BuildingMap buildingMap;

    /**
     * direction to be sent
     */
    private final Move.Direction direction;

    /**
     * coordinates to be sent
     */
    private final Coordinates duCoordinates;


    public DataContainer(BuildingMap buildingMap, Move.Direction direction, Coordinates duCoordinates) {
        this.buildingMap = buildingMap;
        this.direction = direction;
        this.duCoordinates = duCoordinates;
    }

    public DataContainer(BuildingMap buildingMap) {
        this(buildingMap, null, null);
    }

    public BuildingMap getBuildingMap() {
        return this.buildingMap;
    }

    public Move.Direction getDirection() {
        return this.direction;
    }

    public Coordinates getDuCoordinates() {
        return duCoordinates;
    }
}
