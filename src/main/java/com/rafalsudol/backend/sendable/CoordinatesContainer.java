package com.rafalsudol.backend.sendable;

import com.rafalsudol.backend.terrain.Coordinates;

import java.io.Serializable;

/**
 * Class provides container for exchanging coordinates
 *
 * @author Rafal Sudol
 */
public final class CoordinatesContainer implements Serializable {
    /**
     * current coordinates of disposal unit
     */
    private final Coordinates oldCoordinates;

    /**
     * new coordinates for disposal unit's next move
     */
    private final Coordinates newCoordinates;

    public CoordinatesContainer(Coordinates oldCoordinates, Coordinates newCoordinates) {
        this.oldCoordinates = oldCoordinates;
        this.newCoordinates = newCoordinates;
    }

    public Coordinates getOldCoordinates() {
        return new Coordinates(oldCoordinates.getXCor(), oldCoordinates.getYCor());
    }

    public Coordinates getNewCoordinates() {
        return new Coordinates(newCoordinates.getXCor(), newCoordinates.getYCor());
    }
}
