package com.rafalsudol.backend.terrain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Class representing bomb
 *
 * @author Rafal Sudol
 */
public final class Bomb implements Serializable {
    private final Logger logger = LoggerFactory.getLogger(getClass().getName());

    /**
     * time needed to defuse the bomb
     */
    private AtomicInteger requiredTime;

    /**
     * bomb's position
     */
    private final Coordinates position;

    /**
     * defusing difficulty
     * number of disposal units needed to defuse the bomb
     * units have to act simultaneously
     */
    private final Difficulty difficulty;

    /**
     * number of disposal units currently defusing the bomb
     */
    private int defusingUnits;

    private boolean isDefused;

    /**
     * enum representing difficulty
     */
    public enum Difficulty {
        EASY(1),
        MEDIUM(2),
        HARD(3),
        VERY_HARD(4);

        int difficulty;

        Difficulty(int difficulty) {
            this.difficulty = difficulty;
        }

        public int getDifficulty() {
            return this.difficulty;
        }
    }

    public Bomb(AtomicInteger requiredTime, Coordinates position, Difficulty difficulty, boolean isDefused, int defusingUnits) {
        this.requiredTime = requiredTime;
        this.position = position;
        this.difficulty = difficulty;
        this.defusingUnits = defusingUnits;
        this.isDefused = isDefused;
    }

    public Bomb(AtomicInteger requiredTime, Coordinates position, Difficulty difficulty) {
        this(requiredTime, position, difficulty, false, 0);
    }

    public Bomb(Coordinates position) {
        this(new AtomicInteger(20), position, Difficulty.values()[new Random().nextInt(Difficulty.values().length)], false, 0);
    }

    public Bomb(Coordinates position, boolean isDefused) {
        this(new AtomicInteger(20), position, Difficulty.values()[new Random().nextInt(Difficulty.values().length)], isDefused, 0);
    }

    public Difficulty getDifficulty() {
        return this.difficulty;
    }

    public void setDefusingUnits(int defusingUnits) {
        this.defusingUnits = defusingUnits;
    }

    public int getDefusingUnits() {
        return this.defusingUnits;
    }

    /**
     * checks if the defusing process can be started
     *
     * @return true if defusing can be started
     */
    public boolean isDefusingPossible() {
        logger.info("To start defusing " + this.getDifficulty().getDifficulty() + " disposal units is/are needed," +
                " present: " + this.defusingUnits + " units. " +
                "Defusing possible?: " + isDefusingPossible());
        return this.defusingUnits >= this.difficulty.getDifficulty();
    }

    public boolean isDefused() {
        return this.isDefused;
    }

    /**
     * defuses the bomb
     *
     * @return true when the bomb is defused
     */
    //TODO needs validation
    public boolean defuse() {
        if (!this.isDefused) {
            if (this.requiredTime.decrementAndGet() <= 0) {
                logger.info("Defusing...");
                logger.info("Time needed for the defusing to be finished: " + this.requiredTime);
                return false;
            }
        }
        if (this.isDefused) {
            return true;
        }
        return false;
    }

    /**
     * makes a copy of the bomb
     */
    public Bomb copy() {
        AtomicInteger requiredTime = new AtomicInteger(this.requiredTime.intValue());
        Coordinates position = new Coordinates(this.position.getXCor(), this.position.getYCor());

        Bomb bomb = new Bomb(requiredTime, position, this.difficulty, this.isDefused, this.defusingUnits);
        return bomb;
    }
}

