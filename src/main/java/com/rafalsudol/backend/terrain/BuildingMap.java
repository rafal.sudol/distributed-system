package com.rafalsudol.backend.terrain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Class provides program representation of the environment to be examined
 *
 * @author Rafal Sudol
 */
public final class BuildingMap implements Serializable {
    //TODO try to remove harcoding
    private final static Logger logger = LoggerFactory.getLogger(BuildingMap.class);

    /**
     * area to be examined
     */
    private final Point[][] area;

    /**
     * constructor to be used for tests
     * when no GUI is available
     *
     * @param stringMap building map in string format
     */
    public BuildingMap(String[] stringMap) {
        this.area = makeMap(stringMap);
    }

    public BuildingMap(Point[][] area) {
        if (area != null) {
            this.area = area;
        } else {
            throw new IllegalArgumentException("Constructor's argument refers to null");
        }
    }

    //general use constructor
    private BuildingMap(int hSize, int vSize) {
        area = new Point[vSize][hSize];
        for (int y = 0; y < vSize; y++) {
            for (int x = 0; x < hSize; x++) {
                Point point = new Point(x, y, Point.pointState.UNDISCOVERED, null);
                area[y][x] = point;
            }
        }
    }

    public Point[][] getArea() {
        return area;
    }

    /**
     * prints simple visualization of the area to be examined
     * for debugging purpose only
     */
    public void printMap() {
        int colN = 0;
        System.out.print("   ");
        for (int i = 0; i < this.area[0].length; i++) {
            System.out.print(" " + i + " ");
        }
        System.out.println();
        for (Point[] row : this.area) {
            System.out.print(" " + colN + " ");
            colN++;
            for (Point point : row) {
                if (point.getState() == Point.pointState.OBSTACLE) {
                    System.out.print(Point.ANSI_BLUE + " O " + Point.ANSI_RESET);
                } else if (point.getState() == Point.pointState.UNDISCOVERED) {
                    System.out.print(Point.ANSI_YELLOW + " u " + Point.ANSI_RESET);
                } else if (point.getState() == Point.pointState.AGENT) {
                    System.out.print(Point.ANSI_CYAN + " A " + Point.ANSI_RESET);
                } else if (point.getState() == Point.pointState.BOMB) {
                    System.out.print(Point.ANSI_RED + " B " + Point.ANSI_RESET);
                } else if (point.getState() == Point.pointState.DEFUSED_BOMB) {
                    System.out.print(Point.ANSI_BLACK + " b " + Point.ANSI_RESET);
                } else if (point.getState() == Point.pointState.SAFE) {
                    System.out.print(Point.ANSI_WHITE + " s " + Point.ANSI_RESET);
                } else {
                    System.out.print(Point.ANSI_RED + " ! " + Point.ANSI_RESET);
                }
            }
            System.out.println();
        }
        System.out.println();
    }

    /**
     * creates building map, basing on provided String array
     * meant for tests, when no GUI is available
     *
     * @param stringMap map represented by String array
     * @return area of Points
     */
    public Point[][] makeMap(String[] stringMap) {
        Point[][] newMap = new Point[stringMap.length][stringMap[0].length()];
        for (int i = 0; i < stringMap.length; i++) {
            for (int j = 0; j < stringMap[0].length(); j++) {
                switch (stringMap[i].charAt(j)) {
                    case 'X':
                        newMap[i][j] = new Point(j, i, Point.pointState.OBSTACLE, null);
                        break;
                    case 'u':
                        newMap[i][j] = new Point(j, i, Point.pointState.UNDISCOVERED, null);
                        break;
                    case 'A':
                        newMap[i][j] = new Point(j, i, Point.pointState.AGENT, null);
                        break;
                    case 'B':
                        newMap[i][j] = new Point(j, i, Point.pointState.BOMB, new Bomb(new Coordinates(j, i)));
                        break;
                    case 'b':
                        newMap[i][j] = new Point(j, i, Point.pointState.DEFUSED_BOMB, new Bomb(new Coordinates(j, i), true));
                        break;
                    case 's':
                        newMap[i][j] = new Point(j, i, Point.pointState.SAFE, null);
                        break;
                }
            }
        }
        return newMap;
    }

    /**
     * updates this to identical state as a source
     *
     * @param buildingMap source
     */
    public void update(BuildingMap buildingMap) {
        if (buildingMap != null) {
            fillAs(buildingMap, this);
        } else {
            logger.error("Provided argument points to null");
        }
    }

    /**
     * creates a copy of this object
     *
     * @return desired BuildingMap object
     */
    public BuildingMap getCopy() {
        BuildingMap copy = new BuildingMap(new Point[this.area.length][this.area[0].length]);
        for (int i = 0; i < this.area.length; i++) {
            for (int j = 0; j < this.area[0].length; j++) {
                copy.getArea()[i][j] = new Point(this.area[i][j].getCoordinates().getXCor(), this.area[i][j].getCoordinates().getYCor(), this.area[i][j].getState(), this.area[i][j].getBomb() == null ? null : this.area[i][j].getBomb().copy());
            }
        }
        return copy;
    }

    /**
     * sets areas to same states
     *
     * @param source source map
     * @param target target map
     */
    private void fillAs(BuildingMap source, BuildingMap target) {
        if (source != null && target != null) {
            if (source.getArea().length == target.getArea().length) {
                if (source.getArea()[0].length == target.getArea()[0].length) {
                    for (int i = 0; i < source.getArea().length; i++) {
                        for (int j = 0; j < source.getArea()[0].length; j++) {
                            target.getArea()[i][j].setState(source.getArea()[i][j].getState());
                        }
                    }
                } else {
                    logger.error("X dimension sizes mismatch");
                }
            } else {
                logger.error("Y dimension sizes mismatch");
            }
        } else {
            logger.error("Argument(s) point(s) to null");
        }
    }

    /**
     * sets points' state to CHECKED
     */
    public void convertToChecked() {
        for (Point[] row : this.area) {
            for (Point point : row) {
                if (point.getState() != Point.pointState.UNDISCOVERED && point.getState() != Point.pointState.SAFE && point.getState() != Point.pointState.BOMB) {
                    point.setState(Point.pointState.CHECKED);
                }
            }
        }
    }

    /**
     * sets all points' state to UNDISCOVERED
     */
    public void prepareInitial() {
        for (Point[] row : area) {
            for (Point point : row) {
                point.setState(Point.pointState.UNDISCOVERED);
            }
        }
    }

    /**
     * sets all points to initial state
     */
    public void setInitialMapState() {
        for (Point[] row : area) {
            for (Point point : row) {
                point.setFieldsToDefalult();
            }
        }
    }

    /**
     * searches for disposal units on the map
     *
     * @return a List of disposal units coordinates
     */
    public List<Coordinates> checkForAgents() {
        List<Coordinates> dUPositions = new ArrayList<>();
        for (Point[] row : area) {
            for (Point point : row) {
                if (point.getState() == Point.pointState.AGENT) {
                    dUPositions.add(new Coordinates(point.getCoordinates().getXCor(), point.getCoordinates().getYCor()));
                }
            }
        }

        if (dUPositions.size() == 0) {
            logger.error("Something went wrong, no disposal unit found");
            return null;
        } else {
            return dUPositions;
        }
    }

    /**
     * validates size of the area
     *
     * @param size size of the area
     * @return true if size value is correct
     */
    private static boolean validateSize(int size) throws IllegalArgumentException {
        if (size > 0) {
            return true;
        } else {
            throw new IllegalArgumentException("Size has to be greater than O!");
        }
    }

    /**
     * provides with the BuilingMap instance of desired size
     *
     * @param hSize horizontal size of the area(number of columns)
     * @param vSize vertical size of the area(number of rows)
     * @return desired BuildingMap instance
     */
    //TODO rewrite into proper singleton
    public static BuildingMap getInstance(int hSize, int vSize) {
        try {
            validateSize(hSize);
            validateSize(vSize);
            return new BuildingMap(hSize, vSize);
        } catch (IllegalArgumentException e) {
            logger.error("Exception during size validation, size has to be greater than 0!");
            logger.error(e.toString());
            System.exit(1);
        }
        return null;
    }
}
