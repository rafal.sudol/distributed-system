package com.rafalsudol.backend.terrain;

import jade.core.Agent;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * Class represents single point in the 2d space
 * Group of points makes area to be examined
 *
 * @author Rafal Sudol
 */
public final class Point implements Serializable {
    //color constants for use during simple test, when a building map is printed to the console
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    /**
     * coordinates of this point
     */
    private final Coordinates coordinates;

    /**
     * state of this point
     */
    private pointState state;

    /**
     * agent standing at this point
     */
    private Agent du;

    /**
     * bomb placed at this point
     */
    private Bomb bomb;

    /**
     * enum representing point's state
     */
    public enum pointState {
        //general use
        AGENT,
        OBSTACLE,
        UNDISCOVERED,
        SAFE,
        BOMB,
        DEFUSED_BOMB,

        //only for specialized functions
        CHECKED         //used by function searching for undiscovered points
    }

    /**
     * constructor
     *
     * @param xCor       x cooridnate of a point
     * @param yCor       y coordinate of a point
     * @param pointState state of a point
     * @param bomb       bomb set at this point, null allowed
     */
    public Point(int xCor, int yCor, pointState pointState, Bomb bomb) {
        this.coordinates = new Coordinates(xCor, yCor);
        this.state = pointState;
        this.bomb = bomb;
    }

    public void setState(pointState state) {
        this.state = state;
    }

    public pointState getState() {
        return this.state;
    }

    public void setDU(Agent du) {
        if (du != null) {
            this.du = du;
        }
    }

    public Agent getDu() {
        return du;
    }

    public void setBomb(Bomb bomb) {
        if (bomb != null) {
            this.bomb = bomb;
        }
    }

    public Bomb getBomb() {
        return bomb;
    }

    public Coordinates getCoordinates() {
        return this.coordinates;
    }

    /**
     * checks if this point is accesible
     *
     * @return true if this point is accessible
     */
    public boolean isAccessible() {
        return this.state == pointState.UNDISCOVERED || this.state == pointState.SAFE || this.state == pointState.BOMB || this.state == pointState.DEFUSED_BOMB;
    }

    /**
     * checks if this point is undiscovered
     *
     * @return true if this point is undiscovered
     */
    public boolean isUndiscovered() {
        return this.state == pointState.UNDISCOVERED;
    }

    /**
     * lists accessible states(disposal unit can make moves only to accessible points)
     *
     * @return a List of accessible states
     */
    public static List<pointState> accessibleStates() {
        return Arrays.asList(pointState.UNDISCOVERED, pointState.SAFE, pointState.BOMB, pointState.DEFUSED_BOMB);
    }

    /**
     * lists discoverable states
     *
     * @return a list of discoverable states
     */
    public static List<pointState> discoverableStates() {
        return Arrays.asList(pointState.UNDISCOVERED, pointState.BOMB);
    }

    /**
     * lists of states that needs discovering
     *
     * @return a list of states, that needs discovering
     */
    public static List<pointState> undiscoveredState() {
        return Arrays.asList(pointState.UNDISCOVERED);
    }

    /**
     * evaluates if a point has already been checked by algorithm searching for agent's options to move
     *
     * @return true if a point was checked by the algorithm
     */
    public boolean isChecked() {
        return this.state == pointState.CHECKED;
    }

    /**
     * makes point copy
     *
     * @return new point, which is a copy of this point
     */
    public Point copy() {
        int xCor = this.coordinates.getXCor();
        int yCor = this.coordinates.getYCor();
        return new Point(xCor, yCor, state, bomb);
    }

    /**
     * sets default class state
     */
    public void setFieldsToDefalult() {
        state = pointState.UNDISCOVERED;
        du = null;
        bomb = null;
    }
}
