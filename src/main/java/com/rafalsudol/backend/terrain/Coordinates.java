package com.rafalsudol.backend.terrain;

import java.io.Serializable;

/**
 * Class represents coordinates in the 2D space
 *
 * @author Rafal Sudol
 */

public final class Coordinates implements Serializable{
    /**
     * x coordinate
     * */
    private int xCor = -1;

    /**
     * y coordinate
     * */
    private int yCor = -1;

    public Coordinates(int xCor, int yCor){
        validateCordinates(xCor, yCor);
        this.xCor = xCor;
        this.yCor = yCor;
    }

    public void setxCor(int newXCor){
        this.xCor = newXCor;
    }

    public void setyCor(int newYCor){
        this.yCor = newYCor;
    }

    public int getXCor(){
        return this.xCor;
    }

    public int getYCor(){
        return this.yCor;
    }

    /**
     * calculates the distance to a point with provided coordinates
     *
     * @param coordinates coordinates of the point to calculate distance to
     * @return distance
     * */
    public int calcDistance(Coordinates coordinates){
        int dX = coordinates.getXCor() - this.xCor;
        int dY = coordinates.getYCor() - this.yCor;
        int distance = (int) Math.sqrt(dX * dX + dY * dY);
        return distance;
    }

    /**
     * checks if coordinates are the same
     *
     * @param coordinates coordinates of a point to compare coordinates to
     * */
    public boolean sameCoordinates(Coordinates coordinates){
        return calcDistance(coordinates) == 0;
    }

    /**
     * validates coordinates
     *
     * @param xCor x coordinate
     * @param yCor y coordinate
     * @return true if coordinates are valid(greater than 0)
     * */
    private boolean validateCordinates(int xCor, int yCor) throws IllegalArgumentException{
        if (xCor >= 0 && yCor >= 0){
            return true;
        } else {
            throw new IllegalArgumentException("Invalid coordinates");
        }
    }

    @Override
    public String toString() {
        return "xCor: " + this.xCor + ", yCor: " + this.yCor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Coordinates that = (Coordinates) o;

        if (xCor != that.xCor) return false;
        return yCor == that.yCor;
    }

    @Override
    public int hashCode() {
        int result = xCor;
        result = 31 * result + yCor;
        return result;
    }
}
