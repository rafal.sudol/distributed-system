package com.rafalsudol.backend.agents;

import com.rafalsudol.backend.behaviours.disposalUnitBehaviours.Move;
import com.rafalsudol.backend.terrain.BuildingMap;
import com.rafalsudol.backend.behaviours.disposalUnitBehaviours.DUInitialBehaviour;
import com.rafalsudol.backend.terrain.Coordinates;
import com.rafalsudol.backend.utility.Services;
import jade.core.AID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Class represents disposal unit agent.
 * There can be many units in the system.
 * The unit explores the area to be examined looking for bombs.
 * When a bomb is found units defuse it.
 *
 * @author Rafal Sudol
 */
public final class DisposalUnit extends MyAgent {
    private final Logger logger = LoggerFactory.getLogger(getClass().getName());

    /**
     * disposal unit's current position
     */
    private Coordinates position;

    /**
     * disposal unit's destination to go to
     */
    private Coordinates destination;

    /**
     * preferred direction of moving
     */
    private Move.Direction preferredDirection;

    /**
     * disposal unit general state
     */
    private duState state;

    public enum duState {
        /**
         * making a move
         */
        TRYING_TO_MOVE,

        /**
         * waiting for move authorization
         */
        WAITING_FOR_AUTHORIZATION,

        /**
         * making a move
         */
        MOVING

        //TODO add missing states
    }

    @Override
    protected void setup() {
        logger.info("Service registration...");
        Services.registerService(DISPOSAL_SRV_NAME, DISPOSAL_SRV_TYPE, this);
        this.updateAgentList(MANAGING_SRV_NAME, MANAGING_SRV_TYPE, super.allManagers);
        state = duState.TRYING_TO_MOVE;
        addBehaviour(new DUInitialBehaviour(getManagers()));
    }

    @Override
    protected void takeDown() {
        System.out.println("Disposal Unit " + this.getLocalName() + " taken down");
    }

    public final List<AID> getManagers() {
        return new ArrayList<>(super.allManagers);
    }

    public final BuildingMap getDUBuildingMap() {
        return super.getBuildingMap();
    }

    public final Coordinates getDestination() {
        if (this.destination != null) {
            return new Coordinates(this.destination.getXCor(), this.destination.getYCor());
        } else {
            return null;
        }
    }

    public final void setDestination(Coordinates destination) {
        this.destination = destination;
    }

    public final void setPosition(Coordinates position) {
        this.position = position;
    }

    public final void setPreferredDirection(Move.Direction preferredDirection) {
        this.preferredDirection = preferredDirection;
    }

    public final Coordinates getPosition() {
        if (position != null) {
            return new Coordinates(this.position.getXCor(), this.position.getYCor());
        } else {
            return null;
        }
    }

    public final Move.Direction getPreferredDirection() {
        return this.preferredDirection;
    }

    /**
     * checks if the unit has reached its destination point
     *
     * @return true if the unit has reached its destination point
     */
    public final boolean onDestination() {
        if (this.position.equals(this.destination)) {
            this.destination = null;
            return true;
        }
        return false;
    }

    public void setDuState(duState state) {
        this.state = state;
    }

    public duState getDuState() {
        return this.state;
    }
}
