package com.rafalsudol.backend.agents;

import com.rafalsudol.backend.terrain.BuildingMap;
import jade.core.AID;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * class implements abstract agent, which is a base class for a manager and disposal unit used
 * int the Distributed System application.
 *
 * @author Rafal Sudol
 */
public abstract class MyAgent extends Agent {
    private final Logger logger = LoggerFactory.getLogger(getClass().getName());

    /**
     * ID's of all managers
     */
    protected final List<AID> allManagers = new ArrayList<>();

    /**
     * ID's of all disposal units
     */
    protected final List<AID> allDisposalUnits = new ArrayList<>();

    /**
     * map of the area to be examined
     */
    protected BuildingMap buildingMap;

    /**
     * constant to be used by framework describing a service
     */
    public static final String MANAGING_SRV_NAME = "Managing service";

    /**
     * constant to be used by framework describing a service
     */
    public static final String MANAGING_SRV_TYPE = "Managing";

    /**
     * constant to be used by framework describing a service
     */
    public static final String DISPOSAL_SRV_NAME = "Disposal service";

    /**
     * constant to be used by framework describing a service
     */
    public static final String DISPOSAL_SRV_TYPE = "Disposal";

    public final BuildingMap getBuildingMap() {
        return this.buildingMap;
    }

    public final void setBuildingMap(BuildingMap buildingMap) {
        this.buildingMap = buildingMap;
    }

    /**
     * updates agents list
     *
     * @param serviceName  name of the service, that agent provides
     * @param serviceType  type of the service, that agent provides
     * @param listToUpdate list to be updated
     */
    protected final void updateAgentList(String serviceName, String serviceType, List<AID> listToUpdate) {
        DFAgentDescription serviceTemplate = new DFAgentDescription();
        ServiceDescription service = new ServiceDescription();
        service.setType(serviceType);
        serviceTemplate.addServices(service);
        DFAgentDescription[] serviceProviders = null;

        logger.info(this.getLocalName() + " -> looking for the service: " + serviceName);
        try {
            serviceProviders = DFService.search(this, serviceTemplate);
            logger.info("Service found.");
        } catch (FIPAException e) {
            logger.error("Service not found.");
            logger.error(e.toString());
        }

        if (serviceProviders.length == 0) {
            logger.info("Service providers not found:" + serviceName);
            //return null;
        } else {
            for (DFAgentDescription serviceProvider : serviceProviders) {
                if (!listToUpdate.contains(serviceProvider.getName())) {
                    listToUpdate.add(serviceProvider.getName());
                }
            }
        }
    }
}
