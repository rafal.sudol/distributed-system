package com.rafalsudol.backend.agents;

import com.rafalsudol.backend.behaviours.managerBehaviours.ListenForUnits;
import com.rafalsudol.backend.behaviours.managerBehaviours.Supervise;
import com.rafalsudol.backend.terrain.BuildingMap;
import com.rafalsudol.backend.utility.Services;
import com.rafalsudol.gui.controllers.MainController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class represent manager agent, who is the core of the whole system.
 * Manager registers disposal units and sends them all required data.
 *
 * @author Rafal Sudol
 */
public final class Manager extends MyAgent {
    private final Logger logger = LoggerFactory.getLogger(getClass().getName());

    /**
     * main GUI controller
     */
    private MainController mainController;

    @Override
    protected void setup() {
        Object[] arguments = getArguments();
        super.buildingMap = (BuildingMap) arguments[0];
        mainController = (MainController) arguments[1];
        logger.info("Registering managing service...");
        Services.registerService(MANAGING_SRV_NAME, MANAGING_SRV_TYPE, this);

        addBehaviour(new ListenForUnits(this, 2000, super.buildingMap));
        addBehaviour(new Supervise(super.buildingMap, this));

    }

    @Override
    protected void takeDown() {
        logger.info("Deregistering managing service...");
        Services.deregisterService(MANAGING_SRV_NAME, MANAGING_SRV_TYPE, this);
    }

    public MainController getMainController() {
        return this.mainController;
    }
}
