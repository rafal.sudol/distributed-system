package com.rafalsudol.backend;

import com.rafalsudol.backend.terrain.BuildingMap;
import com.rafalsudol.gui.controllers.MainController;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.util.leap.Properties;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * backend main class, which launches all main JADE features
 *
 * @author Rafal Sudol
 */

public final class MySystem {
    private final Logger logger = LoggerFactory.getLogger(getClass().getName());
    private final Runtime jadeRuntime = Runtime.instance();
    private AgentContainer mainContainerContoller;
    private AgentController managerContoller;

    /**
     * launches main JADE container
     * creates Manager agent
     *
     * @param buildingMap    an area to be examined, created with GUI drawing tool
     * @param mainController GUI main controller
     */
    public void go(BuildingMap buildingMap, MainController mainController) {
        logger.info("go method launched");

        //main container creation
        Properties mainContainerProperties = new Properties();
        try {
            mainContainerProperties.load("src/main/resources/jade/MainContainer.properties");
        } catch (IOException e) {
            e.printStackTrace();
        }
        Profile mainContainerProfile = new ProfileImpl(mainContainerProperties);
        mainContainerProfile.setParameter(Profile.CONTAINER_NAME, "MAIN");
        mainContainerContoller = jadeRuntime.createMainContainer(mainContainerProfile);

        //manager creation
        try {
            Object[] mapParam = new Object[2];
            mapParam[0] = buildingMap;
            mapParam[1] = mainController;
            managerContoller = mainContainerContoller.createNewAgent("manager", "com.rafalsudol.backend.agents.Manager", mapParam);
            managerContoller.start();
        } catch (StaleProxyException e) {
            e.printStackTrace();
        }

    }
}
