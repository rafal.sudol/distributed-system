package com.rafalsudol.gui.controllers;

import com.rafalsudol.backend.terrain.Point;
import com.rafalsudol.gui.components.GuiBuildingMap;
import com.rafalsudol.backend.MySystem;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.Pane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * Main controller class for Distributed System Application
 *
 * @author Rafal Sudol
 */
public final class MainController implements Initializable {
    private final Logger logger = LoggerFactory.getLogger(getClass().getName());

    /**
     * reference to graphic representation of the whole system(area, units, etc.)
     */
    private final GuiBuildingMap guiBuildingMap;

    /**
     * a map that holds references to the RadioButtons and their state
     */
    private final Map<RadioButton, Boolean> radioButtonsMap;

    /**
     * main pane representing area consisted of Cells
     */
    @FXML
    private Pane boardPane;

    /**
     * RadioButton that needs to be selected in order to draw obstacles
     */
    @FXML
    private RadioButton obstacleRB;

    /**
     * RadioButton that needs to be selected in order to draw bombs
     */
    @FXML
    private RadioButton bombRB;

    /**
     * RadioButton that needs to be selected in order to draw agents
     */
    @FXML
    private RadioButton agentRB;

    /**
     * RadioButton that needs to be selected in order to erase an object
     */
    @FXML
    private RadioButton eraseRB;

    /**
     * ToggleGroup of obstacleRB, bombRB, agentRB, eraseRB
     */
    @FXML
    private ToggleGroup pointStateToggleGroup;

    /**
     * Button that starts the exploration
     */
    @FXML
    private Button startB;

    /**
     * Button that clears already drawn area
     */
    @FXML
    private Button clearB;

    public MainController() {
        guiBuildingMap = GuiBuildingMap.getInstance();
        radioButtonsMap = new HashMap<>();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        guiBuildingMap.initialize(boardPane, this);
        radioButtonsMap.put(obstacleRB, false);
        radioButtonsMap.put(bombRB, false);
        radioButtonsMap.put(agentRB, false);
        radioButtonsMap.put(eraseRB, false);

        pointStateToggleGroup.selectedToggleProperty().addListener((observableValue, oldToggle, newToggle) -> {
            if (pointStateToggleGroup.getSelectedToggle() != null) {
                if (oldToggle != null) {
                    for (RadioButton rb : radioButtonsMap.keySet()) {
                        if (rb.getUserData().toString().equalsIgnoreCase(oldToggle.getUserData().toString())) {
                            radioButtonsMap.put(rb, false);
                        }
                    }
                }
                for (RadioButton rb : radioButtonsMap.keySet()) {
                    if (rb.getUserData().toString().equalsIgnoreCase(newToggle.getUserData().toString())) {
                        radioButtonsMap.put(rb, true);
                    }
                }
            }
        });
        canStartBeEnabled();
    }

    /**
     * returns obstacleRB state
     *
     * @return obstacleRB state
     */
    public boolean getObstacleRBState() {
        System.out.println("z gettera: " + obstacleRB + " : " + radioButtonsMap.get(obstacleRB));
        return radioButtonsMap.get(obstacleRB);
    }

    /**
     * returns bombRB state
     *
     * @return bombRB state
     */
    public boolean getBombRBState() {
        System.out.println("z gettera: " + bombRB + " : " + radioButtonsMap.get(bombRB));
        return radioButtonsMap.get(bombRB);
    }

    /**
     * returns agentRB state
     *
     * @return agentRB state
     */
    public boolean getAgentRBState() {
        System.out.println("z gettera: " + agentRB + " : " + radioButtonsMap.get(agentRB));
        return radioButtonsMap.get(agentRB);
    }

    /**
     * returns eraseRB state
     *
     * @return eraseRB state
     */
    public boolean getEraseRBState() {
        return radioButtonsMap.get(eraseRB);
    }

    /**
     * Runs the exploration process
     */
    public void startDefusing() {
        logger.info("startDefusing run");
        new Thread(() -> {
            MySystem mySystem = new MySystem();
            mySystem.go(guiBuildingMap.getBuildingMap(), this);
        }).start();
    }

    /**
     * clears already drawn area
     */
    public void clearGUIBuildingMap() {
        logger.info("Clearing already drawn area");
        guiBuildingMap.restoreDefMapsState();
        startB.setDisable(true);
        canStartBeEnabled();
    }

    /**
     * checks if the startB Button can be enabled
     * startB can be enabled only when at least one bomb object and one agent object are present
     */
    private void canStartBeEnabled() {
        new Thread(() -> {
            while (true) {
                logger.info("Checking if Start button can be enabled");
                if (guiBuildingMap.isGuiMapReady()) {
                    startB.setDisable(false);
                    break;
                }
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    logger.error("InterruptedException while checking if Start button can be enabled");
                    logger.error(e.toString());
                }
            }
        }).start();
    }

    /**
     * gets reference to a point which is underlying each GUI Cell
     *
     * @param xCoor x coordinate of the desired point
     * @param yCoor y coordinate of the desired point
     * @return the reference to the desired point
     */
    public Point getOriginalPoint(int xCoor, int yCoor) {
        return guiBuildingMap.getOriginalPoint(xCoor, yCoor);
    }

    public GuiBuildingMap getGuiBuildingMap() {
        return this.guiBuildingMap;
    }
}
