package com.rafalsudol.gui.components;

import com.rafalsudol.backend.terrain.Bomb;
import com.rafalsudol.backend.terrain.Coordinates;
import com.rafalsudol.backend.terrain.Point;

import com.rafalsudol.gui.controllers.MainController;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 * Class that graphically represents single element of the area to be examined
 *
 * @author Rafal Sudol
 */
public final class Cell extends StackPane {
    /**
     * x coordinate
     */
    private int x;

    /**
     * y coordinate
     */
    private int y;

    /**
     * size in pixels
     */
    private int size;

    /**
     * underlying Point object
     */
    private final Point point;

    /**
     * main GUI controller
     */
    private final MainController mainController;

    /**
     * area inside of the Cell object
     */
    private final Rectangle area;

    /**
     * internal Cell obejct color
     */
    private Color fillColor;

    /**
     * border color of the Cell object
     */
    private final Color outlineColor;

    Cell(int x, int y, Point point, int size, MainController mainController) {
        this.x = x;
        this.y = y;
        this.point = point;
        this.size = size;
        this.mainController = mainController;

        area = new Rectangle(size, size);

        //place cell in the proper position on a plane
        setTranslateX(this.x * size);
        setTranslateY(this.y * size);

        //paint Cell's area
        paint();

        outlineColor = Color.BLACK;
        area.setStroke(outlineColor);

        getChildren().addAll(area);

        //setting action listeners listening for mouse clicking
        setOnMouseClicked(e -> {
            if (mainController.getBombRBState()) {
                createBomb();
            }
            if (mainController.getAgentRBState()) {
                createAgent();
            }
            if (mainController.getObstacleRBState()) {
                createObstacle();
            }
            if (mainController.getEraseRBState()) {
                erase();
            }
        });

        //setting action listeners listening for mouse dragging
        setOnMouseDragEntered(e -> {
            if (mainController.getObstacleRBState()) {
                createObstacle();
            }
            if (mainController.getEraseRBState()) {
                erase();
            }
        });

        setOnDragDetected(e -> {
            if (mainController.getObstacleRBState()) {
                createObstacle();
                startFullDrag();
            }
            if (mainController.getEraseRBState()) {
                erase();
                startFullDrag();
            }
        });
    }

    public Point getPoint() {
        return point;
    }

    /**
     * paints Cell's internal area
     */
    void paint() {
        switch (point.getState()) {
            case UNDISCOVERED:
                fillColor = Color.DARKGRAY;
                break;
            case BOMB:
                fillColor = Color.RED;
                break;
            case DEFUSED_BOMB:
                fillColor = Color.BLUE;
                break;
            case SAFE:
                fillColor = Color.GREEN;
                break;
            case AGENT:
                fillColor = Color.YELLOW;
                break;
            case OBSTACLE:
                fillColor = Color.DARKSLATEBLUE;
                break;
            default:
                //debugging purpose only
                fillColor = Color.ORANGE;
                break;
        }

        area.setFill(fillColor);
    }

    /**
     * creates a graphical component accordingly to provided pointState
     */
    private void createComponent(Point.pointState state) {
        point.setState(state);
        mainController.getOriginalPoint(x, y).setState(state);
        paint();
    }

    /**
     * creates an obstacle
     */
    private void createObstacle() {
        createComponent(Point.pointState.OBSTACLE);
    }

    /**
     * creates an agent
     */
    private void createAgent() {
        createComponent(Point.pointState.AGENT);
    }

    /**
     * creates a bomb
     */
    private void createBomb() {
        createComponent(Point.pointState.BOMB);
        Bomb bomb = new Bomb(new Coordinates(x, y));
        point.setBomb(bomb);
        mainController.getOriginalPoint(x, y).setBomb(bomb);
    }

    private void erase() {
        createComponent(Point.pointState.UNDISCOVERED);
        //TODO add fields clearing
    }
}

