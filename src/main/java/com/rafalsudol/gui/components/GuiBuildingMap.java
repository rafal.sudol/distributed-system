package com.rafalsudol.gui.components;

import com.rafalsudol.backend.terrain.BuildingMap;
import com.rafalsudol.backend.terrain.Point;
import com.rafalsudol.gui.controllers.MainController;
import javafx.scene.layout.Pane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class that graphically represents area to be examined
 *
 * @author Rafal Sudol
 */
public final class GuiBuildingMap {
    private final Logger logger = LoggerFactory.getLogger(getClass().getName());

    /**
     * single Cell size in pixels
     */
    private final int CELL_SIZE = 15;

    /**
     * number of columns of cells
     */
    private final int CELLS_HOR_NUM = 70;

    /**
     * number of rows of cells
     */
    private final int CELLS_VER_NUM = 50;

    /**
     * width of the area to be examined
     */
    private final double boardPaneW = CELLS_HOR_NUM * CELL_SIZE;

    /**
     * height of the area to be examined
     */
    private final double boardPaneH = CELLS_VER_NUM * CELL_SIZE;

    /**
     * area represented by a group of Cells
     */
    private Cell[][] board;

    /**
     * reference to program represenation of the area to be examined
     */
    private final BuildingMap buildingMap;

    private GuiBuildingMap() {
        board = new Cell[CELLS_VER_NUM][CELLS_HOR_NUM];
        buildingMap = BuildingMap.getInstance(CELLS_HOR_NUM, CELLS_VER_NUM);
    }

    public final BuildingMap getBuildingMap() {
        return this.buildingMap;
    }

    public Cell[][] getBoard() {
        return board;
    }

    /**
     * initializes boardPane
     *
     * @param boardPane  boarPane to be initialized
     * @param controller GUI main controller
     */
    public void initialize(Pane boardPane, MainController controller) {
        for (int y = 0; y < CELLS_VER_NUM; y++) {
            for (int x = 0; x < CELLS_HOR_NUM; x++) {
                Cell cell = new Cell(x, y, buildingMap.getArea()[y][x].copy(), CELL_SIZE, controller);
                board[y][x] = cell;

                boardPane.getChildren().add(cell);


                boardPane.setMinWidth(boardPaneW);
                boardPane.setMinHeight(boardPaneH);
            }
        }
    }

    /**
     * restores default states of both BuildingMap and GuiBuildingMap objects
     */
    public void restoreDefMapsState() {
        buildingMap.setInitialMapState();
        for (Cell[] row : board) {
            for (Cell cell : row) {
                cell.getPoint().setFieldsToDefalult();
                cell.paint();
            }
        }
    }

    /**
     * checks if bomb is placed in the area
     *
     * @return true if bomb is found
     */
    private boolean isBombPresent() {
        logger.info("Checking for bomb presence");
        for (Point[] row : buildingMap.getArea()) {
            for (Point point : row) {
                if (point.getBomb() != null/*point.getState() == Point.pointState.BOMB*/) {
                    logger.info("A bomb has been found");
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * checks if agent is placed in the area
     *
     * @return true if agent is found
     */
    private boolean isAgentPresent() {
        logger.info("Checking for agent presence");
        for (Point[] row : buildingMap.getArea()) {
            for (Point point : row) {
                if (point.getState() == Point.pointState.AGENT) {
                    logger.info("An agent has been found");
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * checks if the area is ready to start exploration process
     *
     * @return true if the area is ready
     */
    public final boolean isGuiMapReady() {
        if (isAgentPresent() && isBombPresent()) {
            logger.info("The area is ready");
            return true;
        }
        return false;
    }

    /**
     * gets reference to a point which is underlying each GUI Cell
     *
     * @param xCoor x coordinate of the desired point
     * @param yCoor y coordinate of the desired point
     * @return the reference to the desired point
     */
    public final Point getOriginalPoint(int xCoor, int yCoor) {
        return buildingMap.getArea()[yCoor][xCoor];
    }

    /**
     * updates Cell appearance
     */
    public final void repaint() {
        //TODO only points, that have been changed should be repainted
        for (Cell[] row : board) {
            for (Cell cell : row) {
                cell.paint();
            }
        }
    }

    /**
     * provides with reference to the GuiBuildingMap object
     *
     * @return desired object
     */
    //TODO rewrite into proper singleton
    public static GuiBuildingMap getInstance() {
        return new GuiBuildingMap();
    }
}
