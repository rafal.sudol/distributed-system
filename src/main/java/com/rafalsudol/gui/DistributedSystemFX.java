package com.rafalsudol.gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ResourceBundle;

/**
 * GUI basic class
 *
 * @author Rafal Sudol
 */
public class DistributedSystemFX extends Application {
    private final Logger logger = LoggerFactory.getLogger(getClass().getName());

    // The primary window of this application
    private Stage primaryStage;

    public DistributedSystemFX() {
        super();
    }

    /**
     * The application starts here
     *
     * @param primaryStage
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        logger.info("Program starts");
        this.primaryStage = primaryStage;
        configureStage();
        primaryStage.setTitle(ResourceBundle.getBundle("WindowProperties").getString("title"));
        primaryStage.show();
    }

    /**
     * Load the FXML and bundle, create a Scene and put the Scene on Stage.
     */
    private void configureStage() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(DistributedSystemFX.class.getResource("/fxml/Main.fxml"));
            loader.setResources(ResourceBundle.getBundle("WindowProperties"));
            Parent parent = loader.load();
            Scene scene = new Scene(parent);
            primaryStage.setScene(scene);
        } catch (IOException ex) {
            logger.error(/*"Unable to configure stage"*/null, ex);
            System.exit(1);
        }
    }
}
